//
//  ViewController.m
//  CS3260
//
//  Created by Randy Jorgensen on 1/12/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
//                    lbl.text = @"Hello World";
//                    lbl.backgroundColor = [UIColor blueColor];
//                    [self.view addSubview:lbl];
    // Do any additional setup after loading the view, typically from a nib.
    UIScrollView *sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor blueColor];
    [self.view addSubview:sv];
    sv.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3);
    
    UIView* redView = [[UIView alloc]initWithFrame:(CGRectMake)
                    (0, 0, sv.frame.size.width, sv.frame.size.height) ];
    redView.backgroundColor = [UIColor redColor];
    [sv addSubview:redView];
    
    UIView* yellowView = [[UIView alloc]initWithFrame:(CGRectMake)
                    (414, 0, sv.frame.size.width, sv.frame.size.height) ];
    yellowView.backgroundColor = [UIColor yellowColor];
    [sv addSubview:yellowView];
    
    UIView* greenView = [[UIView alloc]initWithFrame:(CGRectMake)
                       (0, 736, sv.frame.size.width, sv.frame.size.height) ];
    greenView.backgroundColor = [UIColor greenColor];
    [sv addSubview:greenView];
    
    UIView* purpleView = [[UIView alloc]initWithFrame:(CGRectMake)
                         (414, 736, sv.frame.size.width, sv.frame.size.height) ];
    purpleView.backgroundColor = [UIColor purpleColor];
    [sv addSubview:purpleView];
    
     UIView* brownView = [[UIView alloc]initWithFrame:(CGRectMake)
                          (828, 736, sv.frame.size.width, sv.frame.size.height) ];
    brownView.backgroundColor = [UIColor brownColor];
   [sv addSubview:brownView];
    
    UIView* orangeView = [[UIView alloc]initWithFrame:(CGRectMake)
                         (0, 1472, sv.frame.size.width, sv.frame.size.height) ];
    orangeView.backgroundColor = [UIColor orangeColor];
    [sv addSubview:orangeView];
    
    UIView* grayView = [[UIView alloc]initWithFrame:(CGRectMake)
                          (414, 1472, sv.frame.size.width, sv.frame.size.height) ];
    grayView.backgroundColor = [UIColor grayColor];
    [sv addSubview:grayView];
    
    UIView* blackView = [[UIView alloc]initWithFrame:(CGRectMake)
                         (828, 1472, sv.frame.size.width, sv.frame.size.height) ];
    blackView.backgroundColor = [UIColor blackColor];
    [sv addSubview:blackView];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


